const mongoose = require('mongoose');
const config = require('./config');
const Photos = require('./models/Photos');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('photos');

    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [user1, user2, user3] = await User.create(
        {
            displayName: 'user-1',
            password: '1234',
        },
        {
            displayName: 'user-2',
            password: '1234',
        },
        {
            displayName: 'user-3',
            password: '1234',
        },
    );

    await Photos.create(
        {
            title: 'my favourite work',
            author: user1,
            image: 'img1.jpg'
        }, {
            title: 'my favourite work',
            author: user1,
            image: 'img2.jpg'
        },
        {
            title: 'my favourite work',
            author: user1,
            image: 'img3.jpg'
        },
        {
            title: 'my favourite work',
            author: user2,
            image: 'img4.jpg'
        },
        {
            title: 'my favourite work',
            author: user2,
            image: 'img5.jpg'
        },
        {
            title: 'my favourite work',
            author: user2,
            image: 'img6.jpg'
        },
        {
            title: 'my favourite work',
            author: user3,
            image: 'img7.jpg'
        },
        {
            title: 'my favourite work',
            author: user3,
            image: 'img8.jpg'
        },
        {
            title: 'my favourite work',
            author: user3,
            image: 'img9.jpg'
        }
    );

    db.close();
});