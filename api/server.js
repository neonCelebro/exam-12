const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const users = require('./app/user');
const photos = require('./app/photo');
const config = require('./config');

const port = 8000;
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
    app.use('/users', users());
    app.use('/photos', photos());
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
