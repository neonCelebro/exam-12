const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const albumSchema = new Schema({
    title: {type: String, required: true},
    author: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    image: {type: String, required: true}
});

const Photos = mongoose.model('Photo', albumSchema);

module.exports = Photos;