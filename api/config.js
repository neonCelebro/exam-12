const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'Gallery'
    },
    facebook: {
        appId: "1105208279644242", // Enter your app ID here
        appSecret: "e4a43b4bc250dd6c2e2f9153bb7ca5c6" // Enter your app secret here
    }
};

