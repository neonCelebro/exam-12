const express = require('express');
const request = require('request-promise-native');

const User = require('../models/User');
const config = require('../config');
const nanoid = require("nanoid");

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const newUser = req.body;
        newUser.token = nanoid(9);
        const user = new User(newUser);

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send({message : error}));
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({displayName:  req.body.displayName});

        if (!user) {
            return res.status(400).send({message: 'Username or password is not correct'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({message: 'Username or password is not correct'});
        }
        user.token = nanoid(9);
        await user.save();
        res.send(user);
    });


    router.post('/facebookLogin', async (req, res) => {
        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.appId}|${config.facebook.appSecret}`;

        try {
            const response = await request(debugTokenUrl);

            const decodedResponse = JSON.parse(response);

            if (decodedResponse.data.error) {
                return res.status(401).send({message: 'Facebook token incorrect'});
            }

            if (req.body.id !== decodedResponse.data.user_id) {
                return res.status(401).send({message: 'Wrong user ID'});
            }

            let user = await User.findOne({facebookId: req.body.id});

            if (!user) {
                user = new User({
                    username: req.body.email,
                    password: nanoid(),
                    facebookId: req.body.id,
                    displayName: req.body.name,
                    avatar: req.body.picture.data.url
                });

                user.generateToken();

                await user.save();
            }

            return res.send(user);

        } catch (error) {
            return res.status(401).send({message: 'Facebook token incorrect or just magic'});
        }
    });

    router.delete('/sessions', async (req, res) => {
        const token = req.get('Token');
        const success = {message: 'Logout success!'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();
        await user.save();

        return res.send(success);
    });


    return router;
};

module.exports = createRouter;