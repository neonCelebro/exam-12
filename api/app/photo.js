const express = require('express');
const nanoid = require('nanoid');
const path = require('path');
const multer = require('multer');
const Photos = require('../models/Photos');
const config = require('../config');
const auth = require('../midldleware/auth');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.post('/',  [auth, upload.single('image')], (req, res) => {
            const photo = req.body;

            if (req.file) {
                photo.image = req.file.filename;
            } else {
                return res.status(400).send({message : 'image is required'})
            }

            const albums = new Photos(photo);
        console.log('not add file', albums);
            albums.save()
                .then(result => res.send(result))
                .catch((e) => res.send(e));
    });

    router.get('/', async (req, res) => {
            try {
                const photos = await Photos.find().populate('author', ['displayName', '_id']);
                res.send(photos);
            }catch (e) {
                res.status(400).send({message : e})
            }
        });

    router.get('/:photoID', (req, res) => {
        if (req.params.photoID) {
            Album.find({author: req.params.photoID}, (error, result) => {
                if (error) res.status(404).send({message: error});
                if (result) res.send(result);
            }).populate('author', ['displayName', '_id'])
        }
    });

    router.get('/:id', (req, res) => {
            Album.findOne({_id: req.params.id}, (error, result) => {
                if (error) res.status(404).send(error);
                if (result) res.send(result);
            }).populate('author')
    });

    router.delete('/:id', (req, res) => {
        Photos.remove({_id: req.params.id})
            .then(product => res.send(product))
            .catch(error => res.status(400).send(error))
    });
    return router;
};

module.exports = createRouter;