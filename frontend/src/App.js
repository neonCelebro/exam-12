import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Layout from "./containers/Layout";
import Gallery from "./containers/GalleryRoom/GalleryRoom";
import CssBaseline from "@material-ui/core/es/CssBaseline/CssBaseline";



class App extends Component {
  render() {
    return (
        <Layout>
            <CssBaseline />
            <Switch>
                <Route path="/register" exact component={Register} />
                <Route path="/" component={Gallery} />
            </Switch>
        </Layout>
    );
  }
}

export default App;