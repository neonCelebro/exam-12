import {
    ADD_FRIEND_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  registerError: null,
    loginError: null,
    user: null,
    friends: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
      case REGISTER_USER_SUCCESS:
        return {...state, user: action.user, registerError: null, loginError: null};
    case REGISTER_USER_FAILURE:
      console.log(action.error);
      return {...state, registerError: action.error};
      case LOGIN_USER_FAILURE:
        return {...state, loginError: action.error, registerError: null};
      case LOGOUT_USER:
        return {...state, user: null};
    default:
      return state;
  }
};

export default reducer;