import {ADD_PHOTOS_SUCCESS, FETCH_PHOTOS_SUCCESS} from "../actions/actionTypes";

const initialState = {
    photos : []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photos: action.photos};
        case ADD_PHOTOS_SUCCESS:
            const photos = state.photos;
            const photo = action.photos;
            photos.push(photo);
            return {...state, photos:photos};
        default:
            return state;
    }
};

export default reducer;