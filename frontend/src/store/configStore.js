import createHistory from "history/createBrowserHistory";
import {routerMiddleware, routerReducer} from "react-router-redux";
import thunkMiddleware  from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";


import usersReducer from "./reducers/users";
import galleryRoomReducer from './reducers/galleryRoom';
import {loadState, saveState} from "./reducers/saveStoreInLocalstorage";

const rootReducer = combineReducers({
    users: usersReducer,
    galleryRoom: galleryRoomReducer,
    routing: routerReducer
});

export const history = createHistory();

const setState = loadState();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducer, setState, enhancers);

store.subscribe(() => {
    saveState({
        users: store.getState().users
    });
});

export default store;

