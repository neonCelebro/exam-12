import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {
    LOGIN_USER_FAILURE, LOGOUT_USER, REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "./actionTypes";

const registerUserSuccess = user => {
    return {type: REGISTER_USER_SUCCESS, user};
};

const registerUserFailure = error => {
    return {type: REGISTER_USER_FAILURE, error};
};

const loginUserFailure = error => {

    return {type: LOGIN_USER_FAILURE, error};
};


export const logoutUser = () => ({type: LOGOUT_USER});

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                dispatch(registerUserFailure(error.response.data));
            }
        );
    };
};


export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                console.log(response.data);
                dispatch(registerUserSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                dispatch(loginUserFailure(error.response.data));
            }
        );
    };
};

export const loginUserWithFacebook = userData => {
    return dispatch => {
        return axios.post('/users/facebookLogin', userData).then(
            response => {
                console.log(response.data);
                dispatch(registerUserSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                dispatch(loginUserFailure(error.response.data));
            }
        );
    };
};

export const fetchLogoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};
        axios.delete('/users/sessions', {headers}).then(
            response => {
                dispatch(logoutUser());
                dispatch(push('/'));
            },
            error => {
                console.log('Could not logout');
            }
        );
    }
};
