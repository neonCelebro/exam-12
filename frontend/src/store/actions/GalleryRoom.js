
import axios from "../../axios-api";
import {ADD_PHOTOS_SUCCESS, FETCH_PHOTOS_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";


const fetchPhotosSucces = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos}
};

const AddPhotosSucces = photos => {
    return {type: ADD_PHOTOS_SUCCESS, photos}
};


export const getPhotos = () => {
    return dispatch => {
        return axios.get('/photos/').then(
            response => {
                console.log(response.data);
                dispatch(fetchPhotosSucces(response.data));
            },
            error => {
                console.log(error)
            }
        );
    };
};

export const deletePhoto = id => {
    return dispatch => {
        return axios.delete(`/photos/${id}`).then(
            response => dispatch(push('/')),
            error => console.log(error.data)
        );
    };
};

export const postPhotos = (data) => {

    return (dispatch, getState) => {

        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.post('/photos/' , data, {headers}).then(
            response => {
                dispatch(AddPhotosSucces(response.data));
            },
            error => {
                console.log(error)
            }
        );
    };
};
