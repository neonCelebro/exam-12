import React, {Component} from 'react';
import './GalleryRoom.css';
import {push} from "react-router-redux";
import {connect} from "react-redux";
import {deletePhoto, getPhotos} from "../../store/actions/GalleryRoom";
import Gallery from "../../components/UI/Gallery/Gallery";
import Modal from "../../components/UI/Modal/Modal";

class GalleryRoom extends Component{

    componentDidMount(){
        this.props.getPhotos()
    }

    state = {
        showModal : false,
    };

    handleClickOpen = (id) => {
        this.setState({showModal: true});
    };

    handleClose = () => {
        this.setState({showModal: false});
    };

    changeHandler = (e) => {
        this.setState({[e.target.name] : e.target.value})
    };

    render(){
        this.props.user? null : this.props.route('register');
        return(
            <div className='chatRoom'>
                <Gallery
                    user={this.props.user}
                    photos={this.props.photos}
                    clickButton={this.props.deletePhoto}
                />
            </div>
        )
    }
};

const mapStateToProps = state => ({
    user: state.users.user,
    photos: state.galleryRoom.photos
});

const mapDispathToProps = dispatch => ({
    route: path => dispatch(push(path)),
    getPhotos : () => dispatch(getPhotos()),
    deletePhoto : id => dispatch(deletePhoto(id))

});

export default connect(mapStateToProps, mapDispathToProps)(GalleryRoom);