import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Alert, Button, Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, PageHeader} from "react-bootstrap";
import {loginUser, loginUserWithFacebook, registerUser} from "../../store/actions/users";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Switch from "@material-ui/core/es/Switch/Switch";
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import RegisterIcon from '@material-ui/icons/Add';
import LoginIcon from '@material-ui/icons/AssignmentInd';
import config from '../../config';


class Register extends Component {

    state = {
        displayName: '',
        password: '',
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    registerButtonHandler = event => {
        event.preventDefault();
        this.props.registerUser(this.state);
    };

    loginButtonHandler = event => {
        event.preventDefault();
        this.props.loginUser(this.state);
    };

    fieldHasError = fieldName => {
        if(this.props.registerError){
            if(this.props.registerError.message.errors){
                return this.props.registerError.message.errors[fieldName];
            } if(this.props.registerError.message.errmsg){
                return this.props.registerError.message.errmsg[fieldName];
            }
        }
    };

    facebookResponse = response => {
        if (response.id) {
            this.props.loginUserWithFacebook(response);
            console.log(response)
        }
    };

    render() {
        return (
            <Fragment>
                <PageHeader style={{borderColor: '#6dac8a'}}>
                    Login with FaceBook
                    <FacebookLoginButton
                        appId={config.facebookAppId}
                        fields="name,email,picture"
                        render={renderProps => (
                            <FormControlLabel
                                style={{marginLeft: '30px'}}
                                control={
                                    <Switch
                                        onChange={renderProps.onClick}
                                    />
                                }
                            />
                        )}
                        callback={this.facebookResponse}
                    />
                </PageHeader>
                <PageHeader style={{borderColor: '#6dac8a'}}>Register new user</PageHeader>
                <Form horizontal>

                    <FormGroup
                        controlId="displayName"
                        validationState={this.fieldHasError('displayName') ? 'error' : null}
                    >
                        {this.props.loginError &&
                        <Alert bsStyle="danger">{this.props.loginError.message}</Alert>
                        }
                        {this.fieldHasError('displayName') &&
                        <Alert bsStyle="danger">{this.props.registerError.message.message}</Alert>
                        }
                        {this.fieldHasError('displayName') &&
                        <Alert bsStyle="danger">{this.props.registerError.message.errmsg}</Alert>
                        }

                        <Col componentClass={ControlLabel} sm={2}>
                            Username
                        </Col>
                        <Col sm={4}>
                            <FormControl
                                type="text"
                                placeholder="Enter username"
                                name="displayName"
                                value={this.state.username}
                                onChange={this.inputChangeHandler}
                                autoComplete="off"
                            />
                            {this.fieldHasError('displayName') &&
                            <HelpBlock>{this.props.registerError.message.errors.message}</HelpBlock>
                            }
                        </Col>
                    </FormGroup>

                    <FormGroup
                        controlId="password"
                        validationState={this.fieldHasError('password') ? 'error' : null}
                    >
                        {this.props.error &&
                        <Alert bsStyle="danger">{this.props.loginError.message}</Alert>
                        }
                        <Col componentClass={ControlLabel} sm={2}>
                            Password
                        </Col>
                        <Col sm={4}>
                            <FormControl
                                type="password"
                                placeholder="Enter password"
                                name="password"
                                value={this.state.password}
                                onChange={this.inputChangeHandler}
                                autoComplete="new-username"
                            />
                            {this.fieldHasError('password') &&
                            <HelpBlock>{this.props.registerError.message.errors.message}</HelpBlock>
                            }
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={3} style={{width: '20%'}}>
                            <Button
                                onClick={this.registerButtonHandler}
                                variant="raised"
                            >
                                Create account
                                <RegisterIcon style={{marginLeft: '5px'}}/>
                            </Button>
                        </Col>
                        <Col smOffset={0} sm={3}>
                            <Button
                                onClick={this.loginButtonHandler}
                                variant="raised"
                            >
                                Log In
                                <LoginIcon style={{marginLeft: '5px'}}/>
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>

        )
    }
}

const mapStateToProps = state => ({
    registerError: state.users.registerError,
    loginError: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData)),
    loginUser: userData => dispatch(loginUser(userData)),
    loginUserWithFacebook: userData => dispatch(loginUserWithFacebook(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);