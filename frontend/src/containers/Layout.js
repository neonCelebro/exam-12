import React from 'react';
import {connect} from 'react-redux';
import Toolbar from "../components/UI/Toolbar/Toolbar";
import {fetchLogoutUser} from "../store/actions/users";
import MuiThemeProvider from "@material-ui/core/es/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/es/styles/createMuiTheme";
import {push} from "react-router-redux";


import  './Layout.css';


const theme = createMuiTheme({
    palette: {
        type: 'dark',
    },
});

const Layout = props => (
    <div className='layout' style={{width: window.innerWidth, height: window.innerHeight}}>
        <MuiThemeProvider theme={theme}>
        <header>
            <Toolbar route={props.route} user={props.user} close={props.closeWS} logout={props.logout}/>
        </header>
        <main className="container">
            {props.children}
        </main>
        </MuiThemeProvider>
    </div>
);

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispathToProps = dispatch => ({
    logout: () => dispatch(fetchLogoutUser()),
    route: path => dispatch(push(path)),
});

export default connect(mapStateToProps, mapDispathToProps)(Layout);