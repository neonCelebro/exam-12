import React, { Component } from 'react';
import {connect} from "react-redux";
import Modal from "../../components/UI/Modal/Modal";
import Typography from "@material-ui/core/es/Typography/Typography";
import TextField from "@material-ui/core/es/TextField/TextField";
import Button from "@material-ui/core/es/Button/Button";
import AddButton from '@material-ui/icons/CloudUpload';
import { withStyles } from '@material-ui/core/styles';
import {postPhotos} from "../../store/actions/GalleryRoom";

const styles = theme => ({
    root: {
        margin: '50px',
        display: 'flex',
        flexDirection: "column",
        flexWrap: 'wrap',
    },
    rightIcon: {
        marginLeft: '20px',
    },
    iconSmall: {
        fontSize: 20,
    },
    formControl: {
        margin: theme.spacing.unit,
        width: 120,
    },
    button: {
        margin: theme.spacing.unit,
        width: '200px'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class AddPhoto extends Component{

    state = {
        title: '',
        image: '',
        author: ''
    };

    handleButtonSend = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.postPhotos(formData);
        this.props.handleClose();
    };

    handleChangeInput = event => {
        this.setState({author: this.props.user._id, [event.target.name]: event.target.value });
    };

    handleChangeFile = event => {
        this.setState({ [event.target.name]: event.target.files[0] });
    };


    render(){

        const { classes } = this.props;

        return(
            <Modal
                open={this.props.open}
                handleClose={this.props.handleClose}
            >
                <form
                    onSubmit={this.handleButtonSend}
                    className={classes.root} autoComplete="off">
                    <div>
                        <Typography color='error' variant='display3' >Add Photo</Typography>
                    </div>
                    <TextField
                        value={this.state.title}
                        onChange={this.handleChangeInput}
                        required
                        name='title'
                        id="name"
                        label="Title"
                        className={classes.textField}
                        margin="normal"

                    />
                    <TextField
                        onChange={this.handleChangeFile}
                        name='image'
                        required
                        type="file"
                        className={classes.textField}
                        helperText="add image album"
                        margin="normal"
                    />
                    <Button
                        type='submit' className={classes.button} variant="raised" color="secondary">
                        Add Photo
                        <AddButton className={classes.rightIcon} />
                    </Button>
                </form>
            </Modal>
        )
    }
};

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispathToProps = dispatch => ({
    postPhotos: data => dispatch(postPhotos(data)),
});

export default connect(mapStateToProps, mapDispathToProps)(withStyles(styles)(AddPhoto));
