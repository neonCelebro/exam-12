import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Modal from "../Modal/Modal";
import Button from "@material-ui/core/es/Button/Button";

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: "100%",
        height: "100%",
    },
    subheader: {
        width: '100%',
    },
});


class Gallery extends Component {

    state = {
        open: false,
        openIs : 0
    };

    handleClickDeleteButton = id => {
        this.props.clickButton(id);
        this.setState({open: false})
    };

    handleClickOpen = (id) => {
        this.setState({open: true, openIs: id});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <GridList cellHeight={160} className={classes.gridList} cols={3}>
                    {this.props.photos.map((tile, id) => (
                            <GridListTile key={tile.img} cols={1}>
                                <img onClick={() => this.handleClickOpen(id)} src={`http://localhost:8000/uploads/${tile.image}`}
                                     alt={tile.title}/>
                            </GridListTile>
                    ))}
                    {this.props.photos.length > 1 ?
                        <Modal
                            open={this.state.open}
                            handleClose={this.handleClose}
                            title={this.props.photos[this.state.openIs].title}
                        >
                            {this.props.user._id === this.props.photos[this.state.openIs].author._id?
                                <Button
                                    onClick={() => this.handleClickDeleteButton(this.props.photos[this.state.openIs]._id)}
                                >
                                    delete
                                </Button> : null
                            }
                            <img
                                style={{width: '100%', height: 'auto'}}
                                src={`http://localhost:8000/uploads/${this.props.photos[this.state.openIs].image}`}/>
                        </Modal> : null
                    }
                </GridList>
            </div>
        );
    }
}

export default withStyles(styles)(Gallery);