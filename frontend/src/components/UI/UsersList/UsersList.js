import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/es/Grid/Grid";
import Typography from "@material-ui/core/es/Typography/Typography";
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";
import ListItemAvatar from "@material-ui/core/es/ListItemAvatar/ListItemAvatar";
import Avatar from "@material-ui/core/es/Avatar/Avatar";
import ListItemText from "@material-ui/core/es/ListItemText/ListItemText";
import UserIcon from '@material-ui/icons/PeopleOutline';
import Button from "@material-ui/core/es/Button/Button";
import AddIcon from '@material-ui/icons/Add';
import FormControl from "react-bootstrap/es/FormControl";
import ControlLabel from "react-bootstrap/es/ControlLabel";
import FormGroup from "react-bootstrap/es/FormGroup";


const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    button: {
        margin: '20px 10px',
    },
    scrollList: {
        position: 'relative',
        overflow: 'auto',
        height: '80%',
    },
    demo: {
        overflow: 'hidden',
        height: '490px',
        margin: '5px',
        backgroundColor: 'rgba(50, 50, 50, 0.6)',
    },
    title: {
        fontSize: '18px',
        margin: '18px 12px 0 12px',
    },
    text: {
        fontSize: '14px'
    }
});

class UsersList extends Component {

    buttonHandler = (e) => {
        e.preventDefault();
        if (this.props.email){
            this.props.addFriend(this.props.email);
        }
    };


    render() {

        const { classes } = this.props;

        return (
                <Grid item md={3}>
                    <Typography variant="title" className={classes.title}>
                        Online users
                    </Typography>
                    <div className={classes.demo}>
                        <List dense={true} className={classes.scrollList}>
                            {this.props.users.map((user) => {
                                return (
                                    <ListItem>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <UserIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            className={classes.text}
                                            primary={user.username}
                                        />
                                    </ListItem>
                                )
                            })}
                        </List>
                        <form style={{margin: '10px 5px 0'}}>
                            <Grid container spacing={0}>
                                <Grid md={9}>
                                    <FormGroup
                                        controlId="formControlsTextarea">
                                        <ControlLabel>Add friend</ControlLabel>
                                        <FormControl
                                            name='email'
                                            onChange={this.props.changeHandler}
                                            value={this.props.value}
                                            type='email'
                                            placeholder="vasa@mail.ru"
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid md={1}>
                                    <Button
                                        mini
                                        onClick={this.buttonHandler}
                                        variant="fab"
                                        color="primary"
                                        aria-label="add"
                                        className={classes.button}>
                                        <AddIcon />
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Grid>
        )
    }
}

export default withStyles(styles)(UsersList);




