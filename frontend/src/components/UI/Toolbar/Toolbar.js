import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import HomeIcon from '@material-ui/icons/Home';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';

import UserMenu from "./UserMenu";
import AddPhoto from "../../../containers/AddForm/AddForm";

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class MenuAppBar extends React.Component {

    state = {
        anchorEl: null,
        open: false
    };

    handleClickOpen = (id) => {
        this.setState({open: true, openIs: id});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleChange = () => {
        !this.props.user && this.props.route('/register');
        this.props.user && this.props.logout();

    };

    render() {

        const { classes } = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <div className={classes.root}>
                <AppBar position="static" color='default'>
                    <Toolbar>
                        <IconButton
                            onClick={() => this.props.route('/')}
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="Menu"
                        >
                            <HomeIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                           Gallery
                        </Typography>
                        <FormGroup>
                            <FormControlLabel
                                control={
                                    <Switch checked={this.props.user? true : false} onChange={this.handleChange} aria-label="LoginSwitch" />
                                }
                                label={
                                    <Typography variant="title" gutterBottom>
                                        {this.props.user ? `Hello! ${this.props.user.displayName}` : 'Logout'}
                                    </Typography>}
                            />
                        </FormGroup>
                        {this.props.user && (
                            <Fragment>
                                <UserMenu
                                    addHandleButton={this.handleClickOpen}
                                    avatar={this.props.user.avatar}
                                    route={this.props.route}
                                />
                                <AddPhoto
                                    open={this.state.open}
                                    handleClose={this.handleClose}
                                />
                            </Fragment>
                        )}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

MenuAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);
